const express = require("express");
const app = express();
const port = 4000;
const fakeData = require("./data.json");
const cors = require("cors");
const bodyParser = require("body-parser");

const db = fakeData;

app.use(cors());

app.use(bodyParser.json());

app.get("/user", async (req, res) => {
  await new Promise((res) => setTimeout(res, 5000));
  res.status(200).json({ name: "userName" });
});

app.get("/list", async (req, res) => {
  const { currentPage = 1, perPage = 10 } = req.query;

  await new Promise((res) => setTimeout(res, 2000));
  res.status(200).json({
    data: fakeData.data.slice(
      (+currentPage - 1) * +perPage,
      (+currentPage - 1) * +perPage + +perPage
    ),
    total: fakeData.data.length,
  });
});

app.post("/list/new", async (req, res) => {
  await new Promise((res) => setTimeout(res, 5000));
  db.data.unshift(req.body);
  res.status(200).send();
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});
