import React from "react";
import { userService } from "./../store/services/UserService";

export const Header = () => {
  const { data = {} } = userService.useGetUserQuery();

  return (
    <header className="header">
      <div className="header-border">
        <div className="input-search">
          <input type="text" placeholder="Search" />
        </div>
        <div className="admin-icon">
          <div className="relative">
            <button>
              {data?.name ? (
                <img
                  src="https://avatars0.githubusercontent.com/u/57622665?s=460&u=8f581f4c4acd4c18c33a87b3e6476112325e8b38&v=4"
                  alt={data.name}
                  title={data.name}
                />
              ) : (
                "Not Authorized"
              )}
            </button>
          </div>
        </div>
      </div>
    </header>
  );
};
