import React, { useState, useEffect } from "react";
import Modal from "@material-ui/core/Modal";
import SaveIcon from "@material-ui/icons/Save";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import LinearProgress from "@material-ui/core/LinearProgress";

import { api } from "./../store/services/UserTableService";


export const AddUserModal = () => {
  const [open, setOpen] = useState(false);

  const [
    updateItem,
    { isLoading: isUpdating, isSuccess, isUninitialized },
  ] = api.useCreateItemMutation();

  useEffect(() => {
    if (!isUninitialized && isSuccess) {
      setOpen(false);
    }
  }, [isUninitialized, isSuccess]);

  const Body = () => (
    <form
      onSubmit={(e) => {
        e.preventDefault();
        const formData = new FormData(e.target);
        updateItem({
          name: formData.get("name"),
          company: formData.get("company"),
          email: formData.get("email"),
        });
      }}
    >
      <div className="modal-body">
        {isUpdating && <LinearProgress />}
        <TextField id="name" name="name" label="Name" variant="outlined" />
        <TextField
          id="company"
          name="company"
          label="Company"
          variant="outlined"
        />
        <TextField id="email" name="email" label="Email" variant="outlined" />
        <Button
          type="submit"
          variant="contained"
          color="primary"
          className="save-btn"
          startIcon={<SaveIcon />}
        >
          Save
        </Button>
      </div>
    </form>
  );

  return (
    <>
      <button type="button" onClick={() => setOpen(true)}>
        Open Modal
      </button>
      <Modal
        open={open}
        onClose={() => setOpen(false)}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        <Body />
      </Modal>
    </>
  );
};
