/* import { QueryClient, QueryClientProvider } from "react-query";
import { UserTable } from "./UserTable/UserTable";
import "./App.css";

const queryClient = new QueryClient();

function App() {
  return (
    <QueryClientProvider client={queryClient}>
      <UserTable />
    </QueryClientProvider>
  );
}

export default App; */

import { store } from "./store";
import { Provider } from "react-redux";
import { UserTable } from "./UserTable/UserTable";
import "./App.css";

const App = () => (
  <Provider store={store}>
    <UserTable />
  </Provider>
);

export default App;
