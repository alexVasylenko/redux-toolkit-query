import React, { useState } from "react";
import DataTable from "react-data-table-component";
import { Header } from "./../components/Header";
import { AddUserModal } from "./../components/Modal";
import { api } from "./../store/services/UserTableService";
import { userService } from "./../store/services/UserService";

const columns = [
  {
    name: "Name",
    selector: "name",
    sortable: true,
  },
  {
    name: "Company",
    selector: "company",
    sortable: true,
  },
  {
    name: "Email",
    selector: "email",
    sortable: true,
  },
];

export const UserTable = () => {
  const [perPage, setPerPage] = useState(10);
  const [currentPage, setCurrentPage] = useState(1);

  const handlePerRowsChange = (newPerPage) => {
    setPerPage(newPerPage);
  };

  const handlePageChange = (page) => {
    setCurrentPage(page);
  };

  const { data: userToken = {} } = userService.useGetUserQuery();

  const { data = {}, error, isLoading, isFetching } = api.useGetItemsQuery(
    {
      perPage,
      currentPage,
    },
    {
      skip: !userToken.name,
    }
  );

  return (
    <div>
      <div className="page-container">
        <div className="page-content">
          <Header />
          <main className="content">
            <div className="content-title">
              <h1>Dashboard</h1>
            </div>
            <AddUserModal />

            <DataTable
              title="Users"
              columns={columns}
              data={data.data}
              progressPending={isFetching}
              pagination
              paginationServer
              paginationTotalRows={data.total}
              selectableRows
              onChangeRowsPerPage={handlePerRowsChange}
              onChangePage={handlePageChange}
            />
          </main>
        </div>
      </div>
    </div>
  );
};
