const GET_TABLE_DATA = "toolkit/tableReducer/GET_TABLE_DATA";
const GET_TABLE_DATA_SUCCESS = "toolkit/tableReducer/GET_TABLE_DATA_SUCCESS";
const GET_TABLE_DATA_ERROR = "toolkit/tableReducer/GET_TABLE_DATA_ERROR";

const defaultState = {
  data: [],
  isLoading: false,
  currentPage: 0,
  rowsPerPage: 10,
  serverError: null,
  total: 0,
};

const reducer = (state = defaultState, action) => {
  switch (action.type) {
    case GET_TABLE_DATA:
      return {
        ...state,
        isLoading: true,
      };
    case GET_TABLE_DATA_SUCCESS:
      return {
        ...state,
        data: action.payload.data,
        isLoading: false,
      };
    case GET_TABLE_DATA_ERROR:
      return {
        ...state,
        serverError: action.payload,
      };
    default:
      return state;
  }
};

export default reducer;

export function loadTableData() {
  return { type: GET_TABLE_DATA };
}

export function loadedTableData(data) {
  return {
    type: GET_TABLE_DATA_SUCCESS,
    payload: {
      data,
    },
  };
}

export function errorTableData(errorText) {
  return {
    type: GET_TABLE_DATA_ERROR,
    payload: errorText,
  };
}

export function getTableFromServer(params) {
  return async (dispatch) => {
    try {
      loadTableData();
      const response = (
        await fetch(
          `http://localhost:4000/list?perPage=${params.perPage}&currentPage=${params.currentPage}`
        )
      ).json();
      dispatch(loadedTableData(response.data));
    } catch (e) {
      errorTableData(e.message);
    }
  };
}
