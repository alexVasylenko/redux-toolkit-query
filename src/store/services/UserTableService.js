import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

export const api = createApi({
  baseQuery: fetchBaseQuery({
    baseUrl: "http://localhost:4000/",
  }),
  tagTypes: ["Items"],
  endpoints: (build) => ({
    getItems: build.query({
      query: ({ perPage, currentPage }) => {
        return { url: `list?perPage=${perPage}&currentPage=${currentPage}` };
      },
      providesTags: ["Items"],
    }),
    createItem: build.mutation({
      query: (data) => ({
        url: `list/new`,
        method: "POST",
        body: data,
      }),
      invalidatesTags: [{ type: "Items" }],
    }),
  }),
});
