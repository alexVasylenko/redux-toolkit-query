import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

export const userService = createApi({
  baseQuery: fetchBaseQuery({
    baseUrl: "http://localhost:4000/",
  }),
  reducerPath: "/user",
  tagTypes: ['User'],
  endpoints: (build) => ({
    getUser: build.query({
      query: () => {
          return ({ url: `user` })
      },
      transformResponse: (response) => {
          return response;
      },
      providesTags: ["User"],
      providesTags: (result, error, id) => [{ type: 'User', id }],
    }),
  }),
});
