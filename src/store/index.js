import { configureStore } from "@reduxjs/toolkit";
import { setupListeners } from "@reduxjs/toolkit/query";
import rootReducer from "./reducers";
import { api } from "./services/UserTableService";
import { userService } from "./services/UserService";

export const store = configureStore({
  reducer: {
    rootReducer: rootReducer,
    [api.reducerPath]: api.reducer,
    [userService.reducerPath]: userService.reducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat(api.middleware, userService.middleware),
});

setupListeners(store.dispatch);